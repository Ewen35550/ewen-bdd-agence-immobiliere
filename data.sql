DROP DATABASE IF EXISTS `agence_immobiliere`;
CREATE DATABASE `agence_immobiliere`;
USE `agence_immobiliere`;

CREATE TABLE `service_type` (
    `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `label` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `accommodation_type` (
    `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `label` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `genders` (
    `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `label` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `users` (
    `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(255) NOT NULL,
    `password` VARCHAR(255) NOT NULL,
    `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` DATETIME NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `address` (
    `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `address` VARCHAR(255) NOT NULL,
    `zipcode` VARCHAR(255) NOT NULL,
    `city` VARCHAR(255) NOT NULL,
    `country` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `owners` (
    `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `lastname` VARCHAR(255) NOT NULL,
    `firstname` VARCHAR(255) NOT NULL,
    `phone` VARCHAR(255) NULL DEFAULT NULL,
    `email` VARCHAR(255) NULL DEFAULT NULL,
    `gender_id` INT(11) UNSIGNED NOT NULL,
    `address_id` INT(11) UNSIGNED NOT NULL,
    `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` DATETIME NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    INDEX `gender_id` (`gender_id`),
    CONSTRAINT `owners_ibfk_1` FOREIGN KEY (`gender_id`) REFERENCES `genders` (`id`),
    CONSTRAINT `owners_ibfk_2` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `accommodation_status` (
    `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `label` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `accommodations` (
    `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `nb_pieces` INT(11) NOT NULL,
    `nb_rooms` INT(11) NOT NULL,
    `surface` DECIMAL(10,2) NOT NULL,
    `price` DECIMAL(10,2) NOT NULL,
    `description` TEXT NULL DEFAULT NULL,
    `accommodation_type_id` INT(11) UNSIGNED NOT NULL,
    `serice_type_id` INT(11) UNSIGNED NOT NULL,
    `owner_id` INT(11) UNSIGNED NOT NULL,
    `address_id` INT(11) UNSIGNED NOT NULL,
    `accommodation_status_id` INT(11) UNSIGNED NOT NULL,
    `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` DATETIME NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    INDEX `accommodation_type_id` (`accommodation_type_id`),
    INDEX `serice_type_id` (`serice_type_id`),
    INDEX `owner_id` (`owner_id`),
    CONSTRAINT `accommodations_ibfk_1` FOREIGN KEY (`accommodation_type_id`) REFERENCES `accommodation_type` (`id`),
    CONSTRAINT `accommodations_ibfk_3` FOREIGN KEY (`serice_type_id`) REFERENCES `service_type` (`id`),
    CONSTRAINT `accommodations_ibfk_2` FOREIGN KEY (`owner_id`) REFERENCES `owners` (`id`),
    CONSTRAINT `accommodations_ibfk_4` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`),
    CONSTRAINT `accommodations_ibfk_5` FOREIGN KEY (`accommodation_status_id`) REFERENCES `accommodation_status` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `picture` (
    `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `path` VARCHAR(255) NOT NULL,
    `comment` TEXT NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE `owners_comments` (
    `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `comment` TEXT NOT NULL,
    `owner_id` INT(11) UNSIGNED NOT NULL,
    `user_id` INT(11) UNSIGNED NOT NULL,
    `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` DATETIME NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    INDEX `owner_id` (`owner_id`),
    INDEX `user_id` (`user_id`),
    CONSTRAINT `owners_comments_ibfk_1` FOREIGN KEY (`owner_id`) REFERENCES `owners` (`id`),
    CONSTRAINT `owners_comments_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `accommodations_comments` (
    `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `comment` TEXT NOT NULL,
    `accommodation_id` INT(11) UNSIGNED NOT NULL,
    `user_id` INT(11) UNSIGNED NOT NULL,
    `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` DATETIME NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    INDEX `accommodation_id` (`accommodation_id`),
    INDEX `user_id` (`user_id`),
    CONSTRAINT `accommodations_comments_ibfk_1` FOREIGN KEY (`accommodation_id`) REFERENCES `accommodations` (`id`),
    CONSTRAINT `accommodations_comments_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `accommodation_type` (`id`, `label`) VALUES (1, 'Appartement'), (2, 'Maison'), (3, 'Villa'), (4, 'Château'), (5, 'Manoir'), (6, 'Chalet'), (7, 'Gîte'), (8, 'Moulin'), (9, 'Ferme'), (10, 'Propriété'), (11, 'Hôtel particulier'), (12, 'Loft'), (13, 'Duplex'), (14, 'Triplex'), (15, 'Atelier'), (16, 'Plateau'), (17, 'Remise'), (18, 'Longère'), (19, 'Grange'), (20, 'Viager'), (21, 'Logement commercial'), (22, 'Autre');
INSERT INTO `service_type` (`id`, `label`) VALUES (1, 'Location'), (2, 'Vente');
INSERT INTO `genders` (`id`, `label`) VALUES (1, 'Monsieur'), (2, 'Madame'), (3, 'Autre');
INSERT INTO `accommodation_status` (`id`, `label`) VALUES (1, 'Brouillon'), (2, 'Dossier en cours'), (3, 'Vendu'), (4, 'Loué');

-- Exercices

SELECT * FROM accommodations
JOIN address ON address.id = logement.address_id
JOIN owners ON owners.id = logement.owner_id
WHERE address.city = 'Paris'
AND owners.lastname = 'Dupont'
AND owners.firstname = 'Jean';

SELECT AVG(A.price), A.updated_at FROM accommodations A
WHERE A.accommodation_status_id = 3
GROUP BY A.updated_at
ORDER BY A.updated_at ASC;